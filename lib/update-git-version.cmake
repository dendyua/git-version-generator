
# extract Git version from annotated tag in format vX.Y.Z
execute_process(
	COMMAND ${GIT_EXECUTABLE} describe --match "v[0-9]*.[0-9]*.[0-9]*" --abbrev=8
	WORKING_DIRECTORY "${GIT_DIR}"
	RESULT_VARIABLE result
	OUTPUT_VARIABLE version
	ERROR_VARIABLE error
)

# if failed by any reason then show message and exit with error
if (NOT ${result} EQUAL 0)
	message(FATAL_ERROR "Git failed with error (${result}): ${error}")
endif()

# if file exists and has exactly same version content then do nothing
if (EXISTS "${VERSION_FILE}")
	file(READ "${VERSION_FILE}" old_version)
	if (old_version STREQUAL version)
		return()
	endif()
endif()

# otherwise overwrite it with new version string
file(WRITE "${VERSION_FILE}" "${version}")
